﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    static class Program
    {


        /// <summary>
        /// Progres＜T＞の使用例
        /// </summary>
        async void progressTest()
        {
            //Progress<T>オブジェクトを作成。コンストラクタ引数でイベントハンドラを渡す。
            Progress<int> progress = new Progress<int>(onProgressChanged);

            //非同期タスク開始
            Task task = Task.Run(
                () =>
                {
                    //非同期処理にProgress<T>オブジェクトを渡す
                    doSometing(progress);
                });

            //非同期タスク完了を待つ
            await task;
        }

        /// <summary>
        /// 非同期タスクの中で実行する処理
        /// </summary>
        /// <param name="iProgress">Report(T)はIProgress＜T＞でないと使えない</param>
        void doSometing(IProgress<int> iProgress)
        {
            for (int done = 1; done <= 100; done++)
            {
                //重たい処理の代わり
                System.Threading.Thread.Sleep(100);

                //進捗報告
                iProgress.Report(done);
            }
        }

        /// <summary>
        /// ProgressChangedイベントのイベントハンドラ
        /// </summary>
        /// <param name="count">Progress＜T＞のT</param>
        void onProgressChanged(int count)
        {
            //渡された達成数をプログレスバーに設定
            progressBar1.Value = count;
        }
    }
}
