﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncodeTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {


            Utf8ToSjis(@"C:\temp\locasma\pins2.txt", @"c:\temp\locasma\out_pins2.txt");

        }

        static int Utf8ToSjis(string fname1, string fname2)
        {
            //ファイル読込み
            FileStream fs1 = new FileStream(fname1, FileMode.Open);

            byte[] data = new byte[fs1.Length];
            fs1.Read(data, 0, data.Length);
            fs1.Close();

            Encoding utf8Enc = Encoding.GetEncoding("UTF-8");
            string utf8str = utf8Enc.GetString(data);

            byte[] bytesData = System.Text.Encoding.GetEncoding("Shift_JIS").GetBytes(utf8str);
            Encoding sjisEnc = Encoding.GetEncoding("Shift-JIS");
            string sjis = sjisEnc.GetString(bytesData);


            FileStream fs2 = new FileStream(fname2, FileMode.Create);

            BinaryWriter bw = new BinaryWriter(fs2, System.Text.Encoding.GetEncoding("Shift_JIS"));

            //1行ずつ


            //出力ファイル
            bw.Write(bytesData);
            bw.Close();
            fs2.Close();

            return 0;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string path = @"c:\temp\locasma\pins2.txt";

            //using (StreamReader sr = new StreamReader(path))
            //{
            //    //This allows you to do one Read operation.
            //    Console.WriteLine(sr.ReadToEnd());
            //}


            string[] lines = File.ReadAllLines(path);

            Console.WriteLine("{0}", lines.Length);

            progressBar1.Maximum = lines.Length;
            progressBar1.Minimum = 0;

        }

        private void button3_Click(object sender, EventArgs e)
        {

            string srcName = @"C:\temp\locasma\pins2.txt";
            string destName = @"c:\temp\locasma\out_pins2.txt";


            //ファイル読込み
            FileStream fs1 = new FileStream(srcName, FileMode.Open);
            int fileSize = (int)fs1.Length;
            fs1.Close();

            progressBar1.Maximum = fileSize/1000;
            progressBar1.Minimum = 0;
            int progressValue = 0;


            const int BUFSIZE = 10240;
            byte[] buf = new byte[BUFSIZE];
            byte[] ZEROARRAY = new byte[BUFSIZE];

            int readSize;

            using (FileStream src = new FileStream(srcName, FileMode.Open, FileAccess.Read))
            using (FileStream dest = new FileStream(destName, FileMode.Create, FileAccess.Write))
            {

                while (true)
                {
                    try
                    {
                        readSize = src.Read(buf, 0, BUFSIZE); // 読み込み
                    }
                    catch
                    {
                        // 読み込みに失敗した場合
                        Console.WriteLine("read error at " + src.Position);

                        if (src.Length - src.Position < BUFSIZE)
                        {
                            readSize = (int)(src.Length - src.Position);
                        }
                        else
                        {
                            readSize = BUFSIZE;
                        }
                        src.Seek(readSize, SeekOrigin.Current);
                        dest.Write(ZEROARRAY, 0, readSize); // 0埋めで書き込み
                        continue;
                    }
                    if (readSize == 0)
                    {
                        progressBar1.Value = progressBar1.Maximum;
                        break; // コピー完了
                    }
                    dest.Write(buf, 0, readSize); // 書き込み
                    progressValue += 10;
                    progressBar1.Value = progressValue;
                }
            }
        }
    }
}
