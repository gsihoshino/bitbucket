﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgressSample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressTest();
        }

        public async void progressTest()

        {
            Progress<int> progress = new Progress<int>(onProgressChanged);

            Task task = Task.Run(
                () =>
                {
                    doSometing(progress);
                });

            await task;
        }

        public void doSometing(IProgress<int> iProgress)
        {
            for (int done = 1; done <= 100; done++)
            {
                //重たい処理の代わり
                System.Threading.Thread.Sleep(100);

                //進捗報告
                iProgress.Report(done);
            }
        }
        public void onProgressChanged(int count)
        {
            //渡された達成数をプログレスバーに設定
            progressBar1.Value = count;
        }

    }
}
